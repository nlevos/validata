using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Validata.API.Controllers;
using Validata.API.Entities;
using AutoMapper;
using Validata.API.Profiles;
using Validata.Data;
using Moq;
using Validata.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Validata.API.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetAllPhoneBooks_ShouldReturnAllPhoneBooks()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<PhoneBookProfile>());
            var mapper = config.CreateMapper();

            var fakeRepository = new Mock<IPhoneBookRepository>();
            var mockPhoneBooks = GetTestPhoneBooks();

            fakeRepository.Setup(x => x.GetAllOrderedBy(true)).Returns(
                mockPhoneBooks);

            var controller = new PhoneBooksController(fakeRepository.Object, mapper);

            // Act
            var result = controller.GetPhoneBooks(true);
            var okResult = result.Result as ObjectResult;

            var returnedPhoneBooks = okResult.Value as IEnumerable<PhoneBookDto>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(okResult);
            Assert.True(okResult is OkObjectResult);
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);
            Assert.IsInstanceOf<IEnumerable<PhoneBookDto>>(okResult.Value);
            Assert.IsNotNull(returnedPhoneBooks);
            Assert.AreEqual(4, returnedPhoneBooks.Count());
        }

        private List<PhoneBook> GetTestPhoneBooks()
        {
            var testPhoneBooks = new List<PhoneBook>();
            testPhoneBooks.Add(new PhoneBook { Id = 1, FirstName = "Nikos", LastName = "Levo", Number = "2104400222", PhoneType = "Work"});
            testPhoneBooks.Add(new PhoneBook { Id = 1, FirstName = "Nikos", LastName = "Levo", Number = "6948110070", PhoneType = "Mobile"});
            testPhoneBooks.Add(new PhoneBook { Id = 1, FirstName = "Nikos", LastName = "Levo", Number = "2254041242", PhoneType = "Home"});
            testPhoneBooks.Add(new PhoneBook { Id = 1, FirstName = "Rania", LastName = "Erimaki", Number = "6972212955", PhoneType = "Mobile"});

            return testPhoneBooks;
        }
    }
}
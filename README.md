# README #

This is a coding task that was given to me by Lefteri for Validata.

### Assumptions ###

* I implemented one entity "PhoneBook". I could do 2, like "Users" and "PhoneNumbers" but I choose to make it simple as it satisfies this task's requirements.
* I am using a field "Id" for the unique key and I calculate is as Max(Id) + 1. So, in order to ensure thread safety, I used a semaphore to "lock" the creation of a new entity and avoid the insertion of duplicate keys.
* I implemented the API documentation using Swashbuckle.
* And, I created a simple Test with nunit and moq, testing the Get Method of the API.

Thank you for this opportunity,
Nikos
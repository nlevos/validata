﻿using AutoMapper;
//using CourseLibrary.API.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Validata.API.Profiles
{
    public class PhoneBookProfile : Profile
    {
        public PhoneBookProfile()
        {
            CreateMap<Entities.PhoneBook, Models.PhoneBookDto>();

            CreateMap<Models.PhoneBookForCreationDto, Entities.PhoneBook>();

            CreateMap<Models.PhoneBookForUpdateDto, Entities.PhoneBook>();
        }
    }
}

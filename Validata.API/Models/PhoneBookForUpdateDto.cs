﻿using System.Collections.Generic;

namespace Validata.API.Models
{
    public class PhoneBookForUpdateDto
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string PhoneType { get; set; }

        public string Number { get; set; }
    }
}

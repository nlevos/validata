﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Validata.API.Entities;
using Validata.API.Models;

namespace Validata.Data
{
    public class PhoneBookRepository : IPhoneBookRepository
    {
        private static SemaphoreSlim _sema = new SemaphoreSlim(1, 1);

        readonly string DatabasePath = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, 
            "Data");

        readonly string _phoneBooksJson;

        public PhoneBookRepository()
        {
            _phoneBooksJson = Path.Combine(DatabasePath, "phoneBooks.json");

            if (!File.Exists(_phoneBooksJson))
            {
                File.AppendAllText(_phoneBooksJson, string.Empty);
            }
        }

        public List<PhoneBook> GetAll()
        {
            return ReadDB();
        }

        public IEnumerable<PhoneBook> GetAllOrderedBy(bool orderByFirstName)
        {
            var phoneBooks = ReadDB();

            if (orderByFirstName)
            {
                return phoneBooks.OrderBy(x => x.FirstName);
            }
            else
            {
                return phoneBooks.OrderBy(x => x.LastName);
            }
        }

        public PhoneBook Get(int phoneBookId)
        {
            return GetPhoneBook(phoneBookId);
        }

        public PhoneBook GetPhoneBook(int phoneBookId)
        {
            var phoneBooks = ReadDB();

            var phoneBook = phoneBooks.FirstOrDefault(x => x.Id == phoneBookId);
            
            return phoneBook;
        }

        public bool PhoneBookExists(int phoneBookId)
        {
            var phoneBook = GetPhoneBook(phoneBookId);

            if (phoneBook == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool PhoneBookExists(PhoneBook phoneBook)
        {
            var phoneBooks = ReadDB();

            if (phoneBook == null)
            {
                return false;
            }

            return phoneBooks.FirstOrDefault(x =>
                x.FirstName == phoneBook.FirstName &&
                x.LastName == phoneBook.LastName &&
                x.Number == phoneBook.Number &&
                x.PhoneType == phoneBook.PhoneType) != null;
        }

        public async Task AddPhoneBook(PhoneBook phoneBook)
        {
            if (phoneBook == null)
            {
                throw new ArgumentNullException(nameof(phoneBook));
            }

            await _sema.WaitAsync();

            var phoneBooks = ReadDB();

            phoneBook.Id = (phoneBooks.Any()) ?
                phoneBooks.Select(x => x.Id).Max() + 1 : 1;

            phoneBooks.Add(phoneBook);

            SaveDB(phoneBooks);

            _sema.Release();

            return;
        }

        public void UpdatePhoneBook(PhoneBook phoneBook)
        {
            if (phoneBook == null)
            {
                throw new ArgumentNullException(nameof(phoneBook));
            }

            if (!PhoneBookExists(phoneBook.Id))
            {
                return;
            }

            var phoneBooks = ReadDB();

            var phoneBookToUpdate = phoneBooks.FirstOrDefault(x => x.Id == phoneBook.Id);

            phoneBookToUpdate.FirstName = phoneBook.FirstName;
            phoneBookToUpdate.LastName = phoneBook.LastName;
            phoneBookToUpdate.Number = phoneBook.Number;
            phoneBookToUpdate.PhoneType = phoneBook.PhoneType;

            SaveDB(phoneBooks);
        }

        public void DeletePhoneBook(int phoneBookId)
        {
            if (!PhoneBookExists(phoneBookId))
            {
                return;
            }

            var phoneBooks = ReadDB();

            var phoneBookToRemove = phoneBooks.FirstOrDefault(x => x.Id == phoneBookId);

            phoneBooks.Remove(phoneBookToRemove);

            SaveDB(phoneBooks);

            return;
        }

        public List<PhoneBook> ReadDB()
        {
            var phoneBooksJson = File.ReadAllText(_phoneBooksJson);
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PhoneBook>>(phoneBooksJson);
            var phoneBooks = (list != null) ? list.ToList()
                : new List<PhoneBook>();

            return phoneBooks;
        }

        public void SaveDB(List<PhoneBook> phoneBooks)
        {
            string phoneBooksText = Newtonsoft.Json.JsonConvert.SerializeObject(phoneBooks, 
                Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(_phoneBooksJson, phoneBooksText);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Validata.API.Entities;
using Validata.API.Models;

namespace Validata.Data
{
    public interface IPhoneBookRepository
    {
        List<PhoneBook> GetAll();

        IEnumerable<PhoneBook> GetAllOrderedBy(bool orderByFirstName);

        PhoneBook Get(int phoneBookId);

        PhoneBook GetPhoneBook(int phoneBookId);

        bool PhoneBookExists(int phoneBookId);

        bool PhoneBookExists(PhoneBook phoneBook);

        Task AddPhoneBook(PhoneBook phoneBook);

        void UpdatePhoneBook(PhoneBook phoneBook);

        void DeletePhoneBook(int phoneBookId);

        List<PhoneBook> ReadDB();

        void SaveDB(List<PhoneBook> phoneBooks);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Validata.API.Entities;
using Validata.API.Models;
using Validata.Data;

namespace Validata.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PhoneBooksController : ControllerBase
    {
        //private readonly PhoneBookService _phoneBookService;
        private readonly IPhoneBookRepository _phoneBookRepository;
        private readonly IMapper _mapper;

        public PhoneBooksController(IPhoneBookRepository phoneBookRepository,
            IMapper mapper)
        {
            _phoneBookRepository = phoneBookRepository ??
                                   throw new ArgumentNullException(nameof(phoneBookRepository));

            _mapper = mapper ??
                      throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all Phone Books ordered
        /// </summary>
        /// <param name="orderByFirstName"> If true, then order by Firstname.
        /// Otherwise order by Lastname </param>
        /// <returns>List of all PhoneBooks</returns>
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<PhoneBookDto>> GetPhoneBooks(bool orderByFirstName = true)
        {
            try
            {
                var result = new List<PhoneBook>();

                var phoneBooksFromRepo = _phoneBookRepository.GetAllOrderedBy(orderByFirstName);

                return Ok(_mapper.Map<IEnumerable<PhoneBookDto>>(phoneBooksFromRepo));
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Get PhoneBook by id
        /// </summary>
        /// <param name="phoneBookId"> Specified id for PhoneBook </param>
        /// <returns> PhoneBook </returns>
        [HttpGet("{phoneBookId}", Name = "GetPhoneBook")]
        public IActionResult GetPhoneBook(int phoneBookId)
        {
            try
            {
                var phoneBookFromRepo = _phoneBookRepository.GetPhoneBook(phoneBookId);

                if (phoneBookFromRepo == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<PhoneBookDto>(phoneBookFromRepo));
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Create Phone Book
        /// </summary>
        /// <param name="phoneBook"> PhoneBook Entity </param>
        /// <returns> Created PhoneBook </returns>
        [HttpPost]
        public async Task<ActionResult<PhoneBookDto>> CreatePhoneBook(PhoneBookForCreationDto phoneBook)
        {
            try
            {
                var phoneBookEntity = _mapper.Map<Entities.PhoneBook>(phoneBook);

                if (_phoneBookRepository.PhoneBookExists(phoneBookEntity))
                {
                    return BadRequest("PhoneBook already exists!");
                }

                await _phoneBookRepository.AddPhoneBook(phoneBookEntity);

                var phoneBookToReturn = _mapper.Map<PhoneBookDto>(phoneBookEntity);
                return CreatedAtRoute("GetPhoneBook",
                    new {phoneBookId = phoneBookToReturn.Id},
                    phoneBookToReturn);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Update Phone Book
        /// </summary>
        /// <param name="phoneBook"> PhoneBook Entity </param>
        /// <returns></returns>
        [HttpPut("{phoneBookId}")]
        public ActionResult<PhoneBookDto> UpdatePhoneBook(int phoneBookId, 
            PhoneBookForUpdateDto phoneBook)
        {
            try
            {
                if (!_phoneBookRepository.PhoneBookExists(phoneBookId))
                {
                    return NotFound($"Could not find PhoneBook with id of {phoneBookId}");
                }

                var phoneBookFromRepo = _phoneBookRepository.GetPhoneBook(phoneBookId);

                _mapper.Map(phoneBook, phoneBookFromRepo);

                _phoneBookRepository.UpdatePhoneBook(phoneBookFromRepo);

                return _mapper.Map<PhoneBookDto>(phoneBookFromRepo);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneBookId"></param>
        /// <returns></returns>
        [HttpDelete("{phoneBookId}")]
        public ActionResult DeletePhoneBook(int phoneBookId)
        {
            try
            {
                var phoneBookFromRepo = _phoneBookRepository.GetPhoneBook(phoneBookId);

                if (phoneBookFromRepo == null)
                {
                    return NotFound($"Could not find PhoneBook with id of {phoneBookId}");
                }

                _phoneBookRepository.DeletePhoneBook(phoneBookFromRepo.Id);

                return Ok();
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}

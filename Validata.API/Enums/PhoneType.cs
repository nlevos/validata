﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Validata.API.Enums
{
    public enum PhoneType
    {
        Work,
        Cellphone,
        Home
    }
}

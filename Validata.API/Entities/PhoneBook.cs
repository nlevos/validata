﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Validata.API.Entities
{
    public class PhoneBook
    {
        public PhoneBook()
        {
        }

        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string PhoneType { get; set; }

        [Required]
        public string Number { get; set; }
    }
}
